const moment = require("moment");

const whoisinfo = require("whois-json");
const cors = require('cors');

const bodyParser = require('body-parser')

var isValidDomain = require("is-valid-domain");

const express = require('express')

const app = express()

app.use(bodyParser.json())

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }))



app.get("/", async(req, res) => {

    // for (var i = 0; i < 10; i++) {
    var domain = req.query.domain;
    // }
    console.log(domain);
    // let url = domain.split(".")[1] + "." + "com";
    // let url = `${domain.split["."][1]}.com`;

    var results = await whoisinfo(domain);

    var date = moment(results.creationDate).format("YYYY-MM-DD");
    var expirationDate = moment(results.registrarRegistrationExpirationDate).format("YYYY-MM-DD");
    var currentDate = moment(new Date()).format("YYYY-MM-DD");

    console.log(date);
    console.log(expirationDate)
    console.log(currentDate);

    var a = moment(date);
    var b = moment(currentDate);

    var years = b.diff(a, "year");
    a.add(years, "years");

    var months = b.diff(a, "months");
    a.add(months, "months");

    var days = b.diff(a, "days");

    var domainAge = years + " years " + months + " months " + days + "days";

    console.log(years);
    console.log(months);
    console.log(days);

    res.status(200).json({

        data: results,
        flag: true,
        date: date,
        expirationDate: expirationDate,
        domainAge: domainAge,
    })
});

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Hearders",
        "Origin, X-Requested-With,Content-Type,Accept,Authorization"
    );
    if (req.method === 'OPTIONS') {
        res.header("Access-Control-Allow-Methods", "PUT,POST,PATCH,DELETE,GET");
        return res.json({});
    }
    next();
})






app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});


module.exports = app;